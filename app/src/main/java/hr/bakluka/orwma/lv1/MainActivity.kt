package hr.bakluka.orwma.lv1

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.EditText
import android.widget.TextView
import android.widget.Toast
import java.lang.Math.pow
import kotlin.math.pow

class MainActivity : AppCompatActivity() {
    private fun calculateBMI(height : Float, mass : Float) : Float  {
        return mass / (height.pow(2))
    }
    override fun onCreate(savedInstanceState: Bundle?) {

        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val textView1 : TextView = findViewById(R.id.textView1)
        val textView2 : TextView = findViewById(R.id.textView2)

        val textInputFullName : EditText = findViewById(R.id.inputFullName)
        val textInputDescription : EditText = findViewById(R.id.inputDescription)

        val textInputHeight : TextView = findViewById(R.id.inputHeight)
        val textInputMass : TextView = findViewById(R.id.inputMass)

        findViewById<Button>(R.id.buttonSubmit).setOnClickListener() {
            textView1.text = textInputFullName.text
            textView2.text = textInputDescription.text
        }

        findViewById<Button>(R.id.buttonCalculate).setOnClickListener() {
            var value : String = calculateBMI(textInputHeight.text.toString().toFloat(), textInputMass.text.toString().toFloat()).toString()
            Toast.makeText(this, value, Toast.LENGTH_LONG).show()
        }

    }
}